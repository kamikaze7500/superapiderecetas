package com.example.aplicaciondeapirecetas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private TextView tvLista;
    private TextView tvListaa;
    private Button tvBoton;
    private TextView tvbuscador;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.tvListaa = (TextView) findViewById(R.id.tvlistaa);
        this.tvLista = (TextView) findViewById(R.id.tvlista);
        this.tvBoton = (Button) findViewById(R.id.tvboton);
        this.tvbuscador = (TextView) findViewById(R.id.tvbuscador);

        this.tvBoton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombre = tvbuscador.getText().toString();
                String url = "https://api.edamam.com/search?q="+nombre+"&app_id=060f01c4&app_key=37ae13856cc890b75228b94b843bcef6";
                Toast.makeText(getApplicationContext(), "Buscando: " + nombre, Toast.LENGTH_SHORT).show();
                StringRequest Solicitud = new StringRequest(
                        Request.Method.GET,
                        url,
                        new Response.Listener<String>() {
                            @Override

                            public void onResponse(String response) {
                                // Tenemos respuesta desde el servidor

                                try {

                                    JSONObject respuestaJSON = new JSONObject(response);

                                    JSONArray histJSON = respuestaJSON.getJSONArray("hits");
                                    JSONObject recetaJSON = histJSON.getJSONObject(0);
                                    JSONObject recipeJSON = recetaJSON.getJSONObject("recipe");
                                    String name = recipeJSON.getString("label");
                                    JSONArray ingredieJSON = recipeJSON.getJSONArray("ingredientLines");
                                    String ingredientes = "";

                                    for (int i = 0; i < ingredieJSON.length(); i++) {
                                        ingredientes = ingredientes + "\n" + ingredieJSON.getString(i);
                                    }

                                    JSONObject nutriJSON = recipeJSON.getJSONObject("totalNutrients");
                                    JSONObject kcalJON = nutriJSON.getJSONObject("ENERC_KCAL");
                                    String aJSON = kcalJON.getString("label");
                                    String aaJSON = kcalJON.getString("quantity");
                                    String aaaJSON = kcalJON.getString("unit");

                                    JSONObject fatJON = nutriJSON.getJSONObject("FAT");
                                    String eJSON = fatJON.getString("label");
                                    String eeJSON = fatJON.getString("quantity");
                                    String eeeJSON = fatJON.getString("unit");

                                    JSONObject satJON = nutriJSON.getJSONObject("FASAT");
                                    String iJSON = satJON.getString("label");
                                    String iiJSON = satJON.getString("quantity");
                                    String iiiJSON = satJON.getString("unit");

                                    JSONObject traJON = nutriJSON.getJSONObject("FATRN");
                                    String oJSON = traJON.getString("label");
                                    String ooJSON = traJON.getString("quantity");
                                    String oooJSON = traJON.getString("unit");

                                    JSONObject fibJON = nutriJSON.getJSONObject("FIBTG");
                                    String uJSON = fibJON.getString("label");
                                    String uuJSON = fibJON.getString("quantity");
                                    String uuuJSON = fibJON.getString("unit");

                                    tvLista.setText(name+"\n"+ingredientes);
                                    tvListaa.setText("Energia kcal \n"+"etiqueta: "+aJSON+"\ncantidad: "+aaJSON+""+aaaJSON+
                                            "\nGrasas \n"+"etiqueta: "+eJSON+"\ncantidad: "+eeJSON+""+eeeJSON+
                                            "\nSaturado \n"+"etiqueta: "+iJSON+"\ncantidad: "+iiJSON+""+iiiJSON+
                                            "\nTrans \n"+"etiqueta: "+oJSON+"\ncantidad: "+ooJSON+""+oooJSON+
                                            "\nFibra \n"+"etiqueta: "+uJSON+"\ncantidad: "+uuJSON+""+uuuJSON);
                                } catch (JSONException e) {

                                    e.printStackTrace();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.i("fallo", error.getMessage());

                            }
                        }
                );

                RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
                listaEspera.add(Solicitud);
            }
        });
    }


}
